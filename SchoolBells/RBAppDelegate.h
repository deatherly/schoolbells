//
//  RBAppDelegate.h
//  SchoolBells
//
//  Created by Denise Eatherly on 5/29/14.
//  Copyright (c) 2014 Robochan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
