//
//  main.m
//  SchoolBells
//
//  Created by Denise Eatherly on 5/29/14.
//  Copyright (c) 2014 Robochan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RBAppDelegate class]));
    }
}
